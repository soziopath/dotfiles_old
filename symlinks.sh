#!/bin/bash

# remove static directories
rm -rf ~/.bashrc
rm -rf ~/.i3/config
rm -rf ~/.config/rofi
rm -rf ~/.config/vifm/colors
rm -rf ~/.config/vifm/scripts
rm -rf ~/.config/vifm/vifmrc
rm -rf ~/.xfiles
rm -rf ~/.urxvt
rm -rf ~/.Xresources

# create symlinks
ln -s ~/dotfiles/bash/.bashrc ~/.bashrc
ln -s ~/dotfiles/.i3/config ~/.i3/config
ln -s ~/dotfiles/.config/rofi ~/.config/rofi
ln -s ~/dotfiles/.config/vifm/colors ~/.config/vifm/colors
ln -s ~/dotfiles/.config/vifm/scripts ~/.config/vifm/scripts
ln -s ~/dotfiles/.config/vifm/vifmrc ~/.config/vifm/vifmrc
ln -s ~/dotfiles/.xfiles ~/.xfiles
ln -s ~/dotfiles/urxvt ~/.urxvt
ln -s ~/dotfiles/urxvt/.Xresources ~/.Xresources
